-- phpMyAdmin SQL Dump
-- version 4.6.6deb4+deb9u2
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Sam 28 Août 2021 à 10:29
-- Version du serveur :  10.1.48-MariaDB-0+deb9u2
-- Version de PHP :  7.3.21-1+0~20200807.66+debian9~1.gbp18a1c2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdClicDroit`
--

-- --------------------------------------------------------

--
-- Structure de la table `Carte`
--

CREATE TABLE `Carte` (
  `idCarte` int(11) NOT NULL,
  `idChantier` int(11) NOT NULL,
  `idPrestation` int(11) NOT NULL,
  `dateJanvier` float NOT NULL,
  `dateFevrier` float NOT NULL,
  `dateMars` float NOT NULL,
  `dateAvril` float NOT NULL,
  `dateMai` float NOT NULL,
  `dateJuin` float NOT NULL,
  `dateJuillet` float NOT NULL,
  `dateAout` float NOT NULL,
  `dateSeptembre` float NOT NULL,
  `dateOctobre` float NOT NULL,
  `dateNovembre` float NOT NULL,
  `dateDecembre` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Carte`
--

INSERT INTO `Carte` (`idCarte`, `idChantier`, `idPrestation`, `dateJanvier`, `dateFevrier`, `dateMars`, `dateAvril`, `dateMai`, `dateJuin`, `dateJuillet`, `dateAout`, `dateSeptembre`, `dateOctobre`, `dateNovembre`, `dateDecembre`) VALUES
(7, 3, 4, 21.67, 21.67, 21.67, 22, 21.67, 21.67, 21.67, 21.67, 21.67, 21.67, 21.67, 21.67),
(8, 3, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3),
(9, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3),
(10, 4, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3);

-- --------------------------------------------------------

--
-- Structure de la table `Chantier`
--

CREATE TABLE `Chantier` (
  `idChantier` int(11) NOT NULL,
  `nameChantier` varchar(255) CHARACTER SET utf8 NOT NULL,
  `codeChantier` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Chantier`
--

INSERT INTO `Chantier` (`idChantier`, `nameChantier`, `codeChantier`) VALUES
(3, 'AM PRODUCTION', 'AMP0301'),
(4, 'ADOMOS', 'ADM0101');

-- --------------------------------------------------------

--
-- Structure de la table `Prestation`
--

CREATE TABLE `Prestation` (
  `idPrestation` int(11) NOT NULL,
  `namePrestation` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `Prestation`
--

INSERT INTO `Prestation` (`idPrestation`, `namePrestation`) VALUES
(4, 'Vitrerie'),
(5, 'Remise en état mensuelle');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Carte`
--
ALTER TABLE `Carte`
  ADD PRIMARY KEY (`idCarte`),
  ADD KEY `idChantier` (`idChantier`),
  ADD KEY `idPrestation` (`idPrestation`);

--
-- Index pour la table `Chantier`
--
ALTER TABLE `Chantier`
  ADD PRIMARY KEY (`idChantier`);

--
-- Index pour la table `Prestation`
--
ALTER TABLE `Prestation`
  ADD PRIMARY KEY (`idPrestation`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Carte`
--
ALTER TABLE `Carte`
  MODIFY `idCarte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `Chantier`
--
ALTER TABLE `Chantier`
  MODIFY `idChantier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `Prestation`
--
ALTER TABLE `Prestation`
  MODIFY `idPrestation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Carte`
--
ALTER TABLE `Carte`
  ADD CONSTRAINT `Carte_ibfk_1` FOREIGN KEY (`idChantier`) REFERENCES `Chantier` (`idChantier`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Carte_ibfk_2` FOREIGN KEY (`idPrestation`) REFERENCES `Prestation` (`idPrestation`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
