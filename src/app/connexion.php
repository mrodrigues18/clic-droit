<?php

function connect($config) {
    try {
        $db = new PDO('mysql:host='. $config['server'] . ';dbname=' . $config['db'], $config['login'], $config['passwd']);
    } catch (Exception $e) {
        echo 'Echec de connexion à la BDD: ' . $e->getMessage();
        $db = null;
    }
    return $db;
}